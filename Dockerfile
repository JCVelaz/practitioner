# Imagen base
FROM node:latest

#Carpeta raíz del contenedor
WORKDIR /app

#Copiado de archivos
ADD / /app/

ADD server.js /app
ADD package.json /app
# ADD  /img /app/img

# Dependencias
RUN npm install

# Puerto que exponemos
EXPOSE 3000

# Comandos para ejecutar la aplicación
CMD ["npm","start"]
